
package it.uniba.gol;

import static org.junit.Assert.*;

import org.junit.Test;

public class GridTest {

	@Test
	public void gridShouldReturnWidth() throws Exception {

		Cell[][] cells = new Cell[2][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
				
		Grid g = new Grid(cells,2,3);
				
		assertEquals(2, g.getWidth());
		
		
	}
	
	@Test
	public void gridShouldReturnHeight() throws Exception {

		Cell[][] cells = new Cell[2][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
				
		Grid g = new Grid(cells,2,3);
				
		assertEquals(3, g.getHeight());
		
		
	}
	
	

}
